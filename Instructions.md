# Instructions for Usage:

To use this project several steps need to be done in order to use this repository:
- [Download](https://git-scm.com/downloads) git to your computer
	- Follow all the instructions and install it
	- You can now right-click in any folder and use the command _Git Bash Here_
- Open the Git Bash anywhere
- First we need to set some global configs:
	- in the git console type:
	- `git config --global user.name "FIRST_NAME LAST_NAME"` with your name/username
	- `git config --global user.email "MY_NAME@example.com"` with your email address, you want to use
- Next we need to generate an ssh key:
	- in your git bash type the following:
	- `ssh-keygen -t rsa -b 4096 -C "your_email@example.com"` with your email address
	- when you're prompted to "Enter a file in which to save the key," press Enter -> this will accepts the default location and name
	- press Enter for the next questions about the passphrase, to skip it
- Now that we've generated and ssh key, we need to add it to you local machine
	- in your git bash type the following:
	- `eval $(ssh-agent -s)` - will print: `Agent pid xxxxx`
	- `ssh-add ~/.ssh/id_rsa` - will add your key to the ssh-agent
- Next step is to create an account on [Gitlab](https://gitlab.com)
- If you registered successfully, on the top right, click under your profile on _Settings_:
	- on the left, navigate to SSH Keys
	- now you need to your public ssh key. You'll find it under `C:\Users\YOUR_NAME\.ssh\id_rsa.pub`
	- open the file and copy its content in the text box, as described on the website
	- enter a title for the key (you need a seperate key for each machine you're using) and confirm
	- now, you've successfully validated your computer for using your git
- Now that we've successfully set up your computer, we can finally add the project to you computer:
	- navigate to a place on your comouter where you want to save the project (e.g. somewhere under `C:\Users\YOUR_NAME\`)
	- create a new folder
	- inside the folder open the git bash and type the following:
	- `git clone git@gitlab.com:Nibuja/avatar_moba.git`
	- a new folder should appear with the content of the online repo
	- this is the place where the files always stay up-to-date
- The last step is to link this repo to your dota addon.
	- to do this navigate to `avatar_moba\content\dota_addons\` and open your normal command line there
	- use the command `mklink /J "YOUR_PATH\Steam\steamapps\common\dota 2 beta\content\dota_addons\NAME_OF_NEW_ADDON" "avatar"` to create
	a new folder inside your dota2 content addon directory
	- navigate to `avatar_moba\game\dota_addons\` and open your normal command line there again
	- use the command `mklink /J "YOUR_PATH\Steam\steamapps\common\dota 2 beta\game\dota_addons\NAME_OF_NEW_ADDON" "avatar"`
	- now we linked your repo with that addon path. Everythin changed in one path, is automatically changed in the other,
	so you can work normally on your dota project there. If you want to publish the changes to the project, you only have to use the
	project path and have both content and game updated
- Now to start the new addon we need to to a little trick:
	- since the new addon, we just created was not created with the workshop tools, we won't be able to select it in the workshop launcher
	- But we can still start it with ease. To do so navigate to a place you like and rightclick:
	- Select New > Create new Shortcut. A new window will popup and asks you to enter the link
	- Now enter: `YOUR_PATH\Steam\steamapps\common\dota 2 beta\game\bin\win64\dota2.exe" -addon NAME_OF_NEW_ADDON -tools -steam`
	- whenever you click that link, the new addon will start

## Thats it
Now all whats left to do, is to get your current files into the repo. I think you need to ask for contribution to the project, so you can write files. If thats done, you can copy in all your files from your old game_mode and push them. IF you need help with git and don't know how to pull, add, commit and push, I'll be helping you.


