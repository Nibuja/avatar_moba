--[[Jinada
	Author: Pizzalol
	Date: 1.1.2015.]]
function Burst( keys )
	local ability = keys.ability
	local cooldown = keys.cooldown
	local caster = keys.caster	
	local modifierName = "modifier_item_burst"

	ability:StartCooldown(cooldown)

	caster:RemoveModifierByName(modifierName) 

	Timers:CreateTimer(cooldown, function()
		ability:ApplyDataDrivenModifier(caster, caster, modifierName, {})
		end)	
end