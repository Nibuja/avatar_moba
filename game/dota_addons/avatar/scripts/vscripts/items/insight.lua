--[[
	CHANGELIST
	09.01.2015 - Standized the variables
]]

--[[
	Author: kritth
	Date: 7.1.2015.
	Increasing stack after each hit
]]
function insight_trigger( keys )
	-- Variables
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	local modifierName = "modifier_insight_stacker"
	local duration = 2
	if target:IsRealHero() then
		if target:HasModifier("modifier_insight_reveal") then
			ability:ApplyDataDrivenModifier( caster, target, "modifier_insight_reveal", { Duration = 4 } )

		else
			if target:HasModifier( modifierName ) then
				local current_stack = target:GetModifierStackCount( modifierName, ability )
				
				ability:ApplyDataDrivenModifier( caster, target, modifierName, { Duration = duration } )
				target:SetModifierStackCount( modifierName, ability, current_stack+1 )


				if current_stack == 1 then
					ability:ApplyDataDrivenModifier( caster, target, "modifier_insight_stacker_2", { Duration = 2 } )
				end
				if current_stack == 2 then
					ability:ApplyDataDrivenModifier( caster, target, "modifier_insight_reveal", { Duration = 2 } )
				end
			else
				ability:ApplyDataDrivenModifier( caster, target, modifierName, { Duration = duration } )
				target:SetModifierStackCount( modifierName, ability, 1 )
				ability:ApplyDataDrivenModifier( caster, target, "modifier_insight_stacker_1", { Duration = 2 } )
			end
		end
	end
end


function cooldown( keys )
	local ability = keys.ability
	local cooldown = keys.cooldown
	local caster = keys.caster	

	ability:StartCooldown(cooldown)	
end